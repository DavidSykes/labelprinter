"""The main application file"""
import os
import sys
import json
import traceback
from src.settings import Settings
from src.factory import Factory
from src.labelprinter import LabelPrinter
from src.reportlabwrapper import ReportLabWrapper
from src.pdfgenerator import PdfGenerator
from src.addressloader import AddressLoader

# pylint: disable=C0103
# pylint: disable=C0111
# pylint: disable=C0325


def SetUpFactory(settingsObject):
    newFactory = Factory()
    newFactory.register('Settings', settingsObject)
    newFactory.register('ReportLabWrapper', ReportLabWrapper())
    newFactory.register('PdfGenerator', PdfGenerator(newFactory))
    newFactory.register('LabelPrinter', LabelPrinter(newFactory))
    return newFactory

def loadSettings():
    if not os.path.isfile("settings.json"):
        raise Exception("Missing file: settings.json")
    return json.load(open("settings.json"))

def loadAddresses(path):
    addressData = ''
    with open(path, 'rb') as addressfile:
        addressData = addressfile.read()
    al = AddressLoader()
    return al.load_addresses(addressData)

if __name__ == '__main__':
    try:
        if len(sys.argv) != 2:
            raise RuntimeError('Address file is missing')
        settings = Settings(loadSettings())
        factory = SetUpFactory(settings)
        addresses = loadAddresses(sys.argv[1])
        factory.fetch('LabelPrinter').printLabels(addresses)
    except RuntimeError as e:
        print(traceback.format_exc())
        print "Program error", e
