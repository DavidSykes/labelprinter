#Copyright ReportLab Europe Ltd. 2000-2016
#see license.txt for license details
#https://bitbucket.org/rptlab/reportlab
__doc__="""
This generates tables showing the 14 standard fonts in both
WinAnsi and MacRoman encodings, and their character codes.
Supply an argument of 'hex' or 'oct' to get code charts
in those encodings; octal is what you need for \\n escape
sequences in Python literals.

usage: standardfonts.py [dec|hex|oct]
"""
__version__='3.3.0'
import sys
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfgen import canvas
import string

label_formats = {'dec':('%d=', 'Decimal'),
                 'oct':('%o=','Octal'),
                 'hex':('0x%x=', 'Hexadecimal')}

def run(mode):

    label_formatter, caption = label_formats[mode]

    for enc in ['MacRoman', 'WinAnsi']:
        canv = canvas.Canvas(
                'StandardFonts_%s.pdf' % enc,
                )
        canv.setPageCompression(0)

        for faceName in pdfmetrics.standardFonts:
            if faceName in ['Symbol', 'ZapfDingbats']:
                encLabel = faceName+'Encoding'
            else:
                encLabel = enc + 'Encoding'

            fontName = faceName + '-' + encLabel
            pdfmetrics.registerFont(pdfmetrics.Font(fontName,
                                        faceName,
                                        encLabel)
                        )

        for byt in range(32, 256):
            col, row = divmod(byt - 32, 32)
            x = 72 + (66*col)
            y = 720 - (18*row)
            canv.setFont('Helvetica', 14)
            canv.drawString(x, y, str(x) + ',' + str(y))

        canv.drawString(0,  841-10, '0,0')
        canv.drawString(595-40, 841-10, '595,0')
        canv.drawString(0,  0+10, '0,841')
        canv.drawString(595-50, 0+10, '595,841')

        canv.showPage()
        canv.save()

if __name__ == '__main__':
    if len(sys.argv)==2:
        mode = string.lower(sys.argv[1])
        if mode not in ['dec','oct','hex']:
            print(__doc__)

    elif len(sys.argv) == 1:
        mode = 'dec'
        run(mode)
    else:
        print(__doc__)