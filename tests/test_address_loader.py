"""Address loader tests"""
import unittest
import sys
sys.path.append('../src')
from addressloader import AddressLoader
# pylint: disable=E0611
# pylint: disable=E0401
# pylint: disable=C0111
# pylint: disable=C0103
# pylint: disable=C0301

class TestAddressLoader2(unittest.TestCase):

    def setUp(self):
        self.address_loader = AddressLoader()

    def test_MissingAddressesThrowsException(self):
        self.assertRaises(RuntimeError, self.address_loader.load_addresses, None)

    def test_NoAddressesThrowsException(self):
        self.assertRaises(RuntimeError, self.address_loader.load_addresses, '')

    def test_SingleLineProducesAddress(self):
        a = self.address_loader.load_addresses('a')
        self.assertEqual(1, len(a))

    def test_MultipleLinesProducesAddress(self):
        a = self.address_loader.load_addresses("a\n1\n2")
        self.assertEqual(1, len(a))
        self.assertEqual('a', a[0][0])
        self.assertEqual('1', a[0][1])
        self.assertEqual('2', a[0][2])

    def test_EmptyLinesSeparatesAddresses(self):
        a = self.address_loader.load_addresses("a\nb\nc\n\nd\ne\nf\n")
        self.assertEqual(2, len(a))
        self.assertEqual(['a', 'b', 'c'], a[0])
        self.assertEqual(['d', 'e', 'f'], a[1])

    def test_MultipleEmptyLinesAreIgnored(self):
        a = self.address_loader.load_addresses("a\nb\nc\n\n\nd\ne\nf\n")
        self.assertEqual(2, len(a))
        self.assertEqual(['a', 'b', 'c'], a[0])
        self.assertEqual(['d', 'e', 'f'], a[1])



if __name__ == '__main__':

    unittest.main()
