import unittest
# pylint: disable=E0611
# pylint: disable=E0401
# pylint: disable=C0111
# pylint: disable=C0103
# pylint: disable=C0301
import sys
sys.path.append('../src')
from unittest.mock import MagicMock
from factory import Factory
from labelprinter import LabelPrinter
unittest.TestLoader.sortTestMethodsUsing = None

class TestLabelPrinter(unittest.TestCase):

    # Setup

    def setUp(self):
        self.mockPdfGenerator = MagicMock()
        self.mockSettings = self.setUpSettings()
        self.setUpFactory()
        self.labelPrinter = LabelPrinter(self.factory)

    def setUpSettings(self):
        s = MagicMock()
        s.left = 10
        s.top = 10
        s.width = 20
        s.height = 15
        s.pageWidth = 100
        s.pageHeight = 400
        return s

    def setUpFactory(self):
        self.factory = Factory()
        self.factory.register('PdfGenerator', self.mockPdfGenerator)
        self.factory.register('Settings', self.mockSettings)

    # Initialise

    def test_PrintLabelsWithNoDataThrowsException(self):
        self.assertRaises(RuntimeError, self.labelPrinter.printLabels, None)

    def test_PrintLabelsWithNoAddressesThrowsException(self):
        self.assertRaises(RuntimeError, self.labelPrinter.printLabels, [])

    # Printing

    def test_PrintLabelsAddsLabelAtSpecifiedLocation(self):
        self.labelPrinter.printLabels([['1']])
        self.mockPdfGenerator.addLabel.assert_called_with(10, 10, ['1'])

    def test_PrintLabelsAddsSecondLabelNextToFirst(self):
        self.labelPrinter.printLabels([['1'], ['2']])
        self.mockPdfGenerator.addLabel.assert_called_with(30, 10, ['2'])

    def test_PrintLabelsAddsThirdLabelNextToSecond(self):
        self.labelPrinter.printLabels([['1'], ['2'], ['3']])
        self.mockPdfGenerator.addLabel.assert_called_with(50, 10, ['3'])

    def test_PrintLabelsWrapsLabelsToNextLineWhenRightOfNextLabelGoesOverThePage(self):
        self.mockSettings.pageWidth = 69
        self.labelPrinter.printLabels([['1'], ['2'], ['3']])
        self.mockPdfGenerator.addLabel.assert_called_with(10, 25, ['3'])

    def test_PrintLabelsFillsThePage(self):
        self.mockSettings.pageWidth = 60
        self.mockSettings.pageHeight = 50
        self.labelPrinter.printLabels([['1'], ['2'], ['3'], ['4']])
        self.mockPdfGenerator.nextPage. assert_not_called()

    def test_PrintLabelsMovesToNextPageWhenPageIsFull(self):
        self.mockSettings.pageWidth = 60
        self.mockSettings.pageHeight = 50
        self.labelPrinter.printLabels([['1'], ['2'], ['3'], ['4'], ['5']])
        self.mockPdfGenerator.nextPage. assert_called_once_with()

    def test_FirstLabelOnSecondPageGoesToTopLeft(self):
        self.mockSettings.pageWidth = 60
        self.mockSettings.pageHeight = 50
        self.labelPrinter.printLabels([['1'], ['2'], ['3'], ['4']])
        self.mockPdfGenerator.reset_mock()

        self.labelPrinter.printLabels([['x']])
        self.mockPdfGenerator.addLabel.assert_called_with(10, 10, ['x'])

    # Finish

    def test_PrintLabalsFinishesByClosingThePage(self):
        self.labelPrinter.printLabels([['1']])
        self.mockPdfGenerator.closePage.assert_called_with()

if __name__ == '__main__':

    unittest.main()
