""" Test the PdfGenerator class """
import unittest
# pylint: disable=E0611
# pylint: disable=E0401
# pylint: disable=C0111
# pylint: disable=C0103
# pylint: disable=C0301
import sys
sys.path.append('../src')
from unittest.mock import MagicMock
from pdfgenerator import PdfGenerator
from factory import Factory

class TestPdfGenerator(unittest.TestCase):
    # Setup

    def setUp(self):
        self.mockReportLabWrapper = MagicMock()
        self.setUpSettings()
        self.setUpFactory()

        self.pg = PdfGenerator(self.factory)

    def setUpFactory(self):
        self.factory = Factory()
        self.factory.register('ReportLabWrapper', self.mockReportLabWrapper)
        self.factory.register('Settings', self.mockSettings)

    def setUpSettings(self):
        self.mockSettings = MagicMock()
        self.mockSettings.lineHeight = 7
        self.mockSettings.filePath = 'pdfpath'
        self.mockSettings.fontSize = 123

    # Initialise

    def test_OnCreationGeneratorCreatesPdfWithFileNameInSettings(self):
        self.mockReportLabWrapper.createPdf.assert_called_once_with('pdfpath', 123)

    # Add label

    def test_GeneratorAddLabelThrowsExceptionIfNoLabelSupplied(self):
        self.assertRaises(TypeError, PdfGenerator.addLabel, 1, 2, None)

    def test_GeneratorAddLabelThrowsExceptionIfLabelIsNotArray(self):
        self.assertRaises(TypeError, PdfGenerator.addLabel, 1, 2, 'not array')

    def test_GeneratorAddLabelCallsReportLabWrapperDrawString(self):
        self.pg.addLabel(1, 2, ['1', '2', '3'])
        self.assertEqual(3, self.mockReportLabWrapper.drawString.call_count)

    def test_GeneratorAddLabelCallsReportLabWrapperDrawStringWithLabelContentsOffsetByPdfHeightCoordinate(self):
        self.mockReportLabWrapper.page_height = 100
        self.pg.addLabel(1, 2, ['1', '2', '3'])
        self.assertEqual(3, self.mockReportLabWrapper.drawString.call_count)
        self.mockReportLabWrapper.drawString.assert_any_call(1, 100-2, '1')
        self.mockReportLabWrapper.drawString.assert_any_call(1, 100-9, '2')
        self.mockReportLabWrapper.drawString.assert_any_call(1, 100-16, '3')

    # nextPage

    def test_GeneratorNextPageCallsReportLabWrapperNextPage(self):
        self.pg.nextPage()
        self.mockReportLabWrapper.nextPage.assert_called_once_with()

    # closePage

    def test_GeneratorCloseCallsReportLabWrapperClosePage(self):
        self.pg.closePage()
        self.mockReportLabWrapper.closePage.assert_called_once_with()

if __name__ == '__main__':

    unittest.main()
