import unittest
import sys
sys.path.append('../src')
unittest.TestLoader.sortTestMethodsUsing = None
from unittest.mock import MagicMock
from factory import Factory

class TestFactory(unittest.TestCase):

    def setUp(self):
        self.factory = Factory()

    def test_RegisteringAnObjectRegistersThatObject(self):
        self.factory.register("object", 42)
        self.assertEqual(42, self.factory.fetch("object"))

    def test_RequestingUnregisteredObjectRaisesAnException(self):
        self.assertRaises(RuntimeError, self.factory.fetch, "NothingToSeeHere")

if __name__ == '__main__':

    unittest.main()