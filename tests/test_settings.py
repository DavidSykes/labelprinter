"""Test the code that loads the settings"""

import unittest
# pylint: disable=E0611
# pylint: disable=E0401
# pylint: disable=C0111
# pylint: disable=C0103
# pylint: disable=C0301

import sys
sys.path.append('../src')
import json
from settings import Settings, SettingsException

class TestSettings(unittest.TestCase):

    # Setup

    def setUp(self):
        self.json = '{"Left":10,"Top":20,"Width":30,"Height":40,"FilePath":"fpath", "PageWidth":50, "PageHeight":200, "LineHeight":60, "FontSize":12}'
        self.sdata = json.loads(self.json)

    # Initialise

    def test_emptySettingsThrowsException(self):
        self.assertRaises(SettingsException, Settings, None)

    def test_blankSettingsThrowsException(self):
        self.assertRaises(SettingsException, Settings, '')

    def test_invalidSettingsThrowsException(self):
        self.assertRaises(SettingsException, Settings, 'sfafdsa')

    def test_LeftValueIsRequiredNumericAndPositive(self):
        self.valueIsRequired(self.sdata, 'Left')
        self.valueMustBeNumeric(self.sdata, 'Left')
        self.valueMustBePositive(self.sdata, 'Left')

    def test_LeftValueGetsSet(self):
        self.assertEqual(10, Settings(self.sdata).left)

    def test_PageWidthValueIsRequiredNumericAndPositive(self):
        sourceName = 'PageWidth'
        self.valueIsRequired(self.sdata, sourceName)
        self.valueMustBeNumeric(self.sdata, sourceName)
        self.valueMustBePositive(self.sdata, sourceName)

    def test_PageWidthValueGetsSet(self):
        self.assertEqual(50, Settings(self.sdata).pageWidth)

    def test_LineHeightValueIsRequiredNumericAndPositiveNotZero(self):
        sourceName = 'LineHeight'
        self.valueIsRequired(self.sdata, sourceName)
        self.valueMustBeNumeric(self.sdata, sourceName)
        self.valueMustBePositive(self.sdata, sourceName)
        self.valueMustBeNotZero(self.sdata, sourceName)

    def test_LineHeightValueGetsSet(self):
        self.assertEqual(60, Settings(self.sdata).lineHeight)

    def test_FilePathValueIsRequiredString(self):
        sourceName = 'FilePath'
        self.valueIsRequired(self.sdata, sourceName)
        self.lengthMustBeNotZero(self.sdata, sourceName)

    def test_FilePathValueIsSet(self):
        self.assertEqual('fpath', Settings(self.sdata).filePath)

    def valueIsRequired(self, settings, name):
        del self.sdata[name]
        self.assertRaises(SettingsException, Settings, self.sdata)

    def valueMustBePositive(self, settings, name):
        self.sdata[name] = -1
        self.assertRaises(SettingsException, Settings, self.sdata)

    def valueMustBeNumeric(self, settings, name):
        self.sdata[name] = 'a'
        self.assertRaises(SettingsException, Settings, self.sdata)

    def valueMustBeNotZero(self, settings, name):
        self.sdata[name] = 0
        self.assertRaises(SettingsException, Settings, self.sdata)

    def lengthMustBeNotZero(self, settings, name):
        self.sdata[name] = ''
        self.assertRaises(SettingsException, Settings, self.sdata)


    def test_settingsWithNoTopValueThrowsException(self):
        del self.sdata['Top']
        self.assertRaises(SettingsException, Settings, self.sdata)

    def test_settingsWithNonNumericTopValueThrowsException(self):
        self.sdata['Top'] = 'a'
        self.assertRaises(SettingsException, Settings, self.sdata)

    def test_settingsWithNegativeTopValueThrowsException(self):
        self.sdata['Top'] = -1
        self.assertRaises(SettingsException, Settings, self.sdata)

    def test_settingsWithValidTopValueSetsTopValue(self):
        self.sdata['Top'] = 43
        s = Settings(self.sdata)
        self.assertEqual(43, s.top)

    def test_settingsWithNoWidthValueThrowsException(self):
        del self.sdata['Width']
        self.assertRaises(SettingsException, Settings, self.sdata)

    def test_settingsWithNonNumericWidthValueThrowsException(self):
        self.sdata['Width'] = 'a'
        self.assertRaises(SettingsException, Settings, self.sdata)

    def test_settingsWithNegativeWidthValueThrowsException(self):
        self.sdata['Width'] = -1
        self.assertRaises(SettingsException, Settings, self.sdata)

    def test_settingsWithValidWidthValueSetsWidthValue(self):
        self.sdata['Width'] = 44
        s = Settings(self.sdata)
        self.assertEqual(44, s.width)

    def test_settingsWithNoHeightValueThrowsException(self):
        del self.sdata['Height']
        self.assertRaises(SettingsException, Settings, self.sdata)

    def test_settingsWithNonNumericHeightValueThrowsException(self):
        self.sdata['Height'] = 'a'
        self.assertRaises(SettingsException, Settings, self.sdata)

    def test_settingsWithNegativeHeightValueThrowsException(self):
        self.sdata['Height'] = -1
        self.assertRaises(SettingsException, Settings, self.sdata)

    def test_settingsWithValidHeightValueSetsHeightValue(self):
        self.sdata['Height'] = 45
        s = Settings(self.sdata)
        self.assertEqual(45, s.height)

if __name__ == '__main__':

    unittest.main()
