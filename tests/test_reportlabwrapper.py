"""Create a test pdf using the ReportLabWrapper"""

import sys
sys.path.append('../src')
from reportlabwrapper import ReportLabWrapper



if __name__ == '__main__':

    RW = ReportLabWrapper()
    RW.createPdf('test.pdf', 14)
    RW.drawString(100, 700, 'hello world')
    RW.nextPage()
    RW.drawString(100, 700, 'hello world again')
    RW.closePage()
