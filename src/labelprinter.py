
# pylint: disable=C0111
# pylint: disable=C0103


class LabelPrinter(object):
    def __init__(self, factory):
        self.pdfGenerator = factory.fetch('PdfGenerator')
        self.settings = factory.fetch('Settings')
        self.next_label_x = None
        self.next_label_y = None
        self.new_page_is_required = False
        self.reset_top_left()

    def reset_top_left(self):
        self.next_label_x = self.settings.left
        self.next_label_y = self.settings.top

    def printLabels(self, addresses):
        if addresses is None or len(addresses) < 1:
            raise RuntimeError('No addresses to print')

        for a in addresses:
            self.check_for_new_page_required()
            self.print_current_label(a)
            self.move_to_next_label_position()
            self.check_for_wrap_to_next_line()
            self.check_for_wrap_to_next_page()

        self.pdfGenerator.closePage()

    def print_current_label(self, label):
        self.pdfGenerator.addLabel(self.next_label_x, self.next_label_y, label)

    def move_to_next_label_position(self):
        self.next_label_x = self.next_label_x + self.settings.width

    def check_for_wrap_to_next_line(self):
        if self.next_label_x + self.settings.width > self.settings.pageWidth:
            self.next_label_x = self.settings.left
            self.next_label_y = self.next_label_y + self.settings.height

    def check_for_wrap_to_next_page(self):
        if self.next_label_y + self.settings.height > self.settings.pageHeight:
            self.reset_top_left()
            self.new_page_is_required = True

    def check_for_new_page_required(self):
        if self.new_page_is_required:
            self.pdfGenerator.nextPage()
            self.new_page_is_required = False
