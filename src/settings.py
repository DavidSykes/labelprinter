"""Handle the application settings"""
# pylint: disable=E0611
# pylint: disable=E0401
# pylint: disable=C0111
# pylint: disable=C0103
# pylint: disable=C0301


class SettingsException(Exception):
    pass

def getRequiredValue(data, name):
    if not name in data or not data[name]:
        raise SettingsException('Missing value for ' + name)

    return data[name]

def getCheckedPositiveInteger(data, name):
    v = getRequiredValue(data, name)

    if not isinstance(v, (int)):
        raise SettingsException(v + ' is an invalid value for ' + name)
    if v < 0:
        raise SettingsException('The value for ' + name + ' can not be negative')
    return v

class Settings(object):
    def __init__(self, data):
        if not data:
            raise SettingsException('Invalid settings data')
        self.left = getCheckedPositiveInteger(data, 'Left')
        self.top = getCheckedPositiveInteger(data, 'Top')
        self.width = getCheckedPositiveInteger(data, 'Width')
        self.height = getCheckedPositiveInteger(data, 'Height')
        self.pageWidth = getCheckedPositiveInteger(data, 'PageWidth')
        self.pageHeight = getCheckedPositiveInteger(data, 'PageHeight')
        self.lineHeight = getCheckedPositiveInteger(data, 'LineHeight')
        self.filePath = getRequiredValue(data, 'FilePath')
        self.fontSize = getRequiredValue(data, 'FontSize')

