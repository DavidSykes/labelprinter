# pylint: disable=C0111



class Factory(object):
    def __init__(self):
        self.widgets = {}

    def register(self, name, obj):
        self.widgets[name] = obj

    def fetch(self, name):
        try:
            return self.widgets[name]
        except KeyError:
            raise RuntimeError("Factory object %s not found" % (name))
