# pylint: disable=E0611
# pylint: disable=E0401
# pylint: disable=C0111
# pylint: disable=C0103
# pylint: disable=C0301


class AddressLoader(object):
    def __init__(self):
        self.addresses = None
        self.startNewAddress = True
        self.currentAddress = None

    def load_addresses(self, addressData):
        if addressData is None or len(addressData) < 1:
            raise RuntimeError('No addresses to load')

        lines = str.splitlines(addressData)
        self.addresses = []
        self.startNewAddress = True
        for l in lines:
            if len(l) > 0:
                self.addLineToAddress(l)
            else:
                self.markAddressAsComplete()
        return self.addresses

    def addLineToAddress(self, line):
        self.createNewAddressIfRequired()
        self.addresses[self.currentAddress].append(line)

    def createNewAddressIfRequired(self):
        if self.startNewAddress:
            self.addresses.append([])
            self.currentAddress = len(self.addresses) - 1
            self.startNewAddress = False

    def markAddressAsComplete(self):
        self.startNewAddress = True
