

# pylint: disable=C0111
# pylint: disable=C0103


class PdfGenerator(object):
    """PdfGenerator creates the pdf page and adds labels to specified coordinates"""
    def __init__(self, factory):
        self.reportLabWrapper = factory.fetch("ReportLabWrapper")
        self.settings = factory.fetch("Settings")
        self.reportLabWrapper.createPdf(self.settings.filePath, self.settings.fontSize)

    def addLabel(self, left, top, label):
        """Add a label to a location"""
        lineHeight = self.settings.lineHeight
        pageHeight = self.reportLabWrapper.page_height
        for line in label:
            self.reportLabWrapper.drawString(left, pageHeight - top, line)
            top = top + lineHeight

    def nextPage(self):
        self.reportLabWrapper.nextPage()

    def closePage(self):
        """When the page is complete, close it"""
        self.reportLabWrapper.closePage()
