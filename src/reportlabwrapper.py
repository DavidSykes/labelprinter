"""
A wrapper for the report lab library, to allow testing.
"""

from reportlab.pdfgen import canvas

# pylint: disable=C0103
# pylint: disable=C0325
# pylint: disable=C0111
# pylint: disable=W0201


#pdfPageWidth = 595
#pdfPageHeight = 841

class ReportLabWrapper(object):

    def __init__(self):
        self.fontSize = 10
        self.page_height = 841

    def createPdf(self, fileName, fontSize):
        print('create pdf', fileName)
        self.canvas = canvas.Canvas(fileName)
        self.canvas.setPageCompression(0)

        self.fontSize = fontSize
        self.setStyles()

    def setStyles(self):
        self.canvas.setFont("Courier", self.fontSize)

    def drawString(self, x, y, s):
        print('drawString', s)
        self.canvas.drawString(x, y, s)

    def nextPage(self):
        print('nextPage')
        self.canvas.showPage()
        self.setStyles()

    def closePage(self):
        print('closePage')
        self.canvas.showPage()
        self.canvas.save()

